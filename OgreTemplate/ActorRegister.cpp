#include "ActorRegister.h"

map<Ogre::MovableObject *, Actor *> ActorRegister::mRegister;
ActorRegister::ActorRegister(void)
{
}

ActorRegister::~ActorRegister(void)
{
}

void ActorRegister::addActor(Ogre::MovableObject * obj, Actor *act)
{
	map<Ogre::MovableObject *, Actor *>::iterator itr;
	if((itr = mRegister.find(obj)) == mRegister.end())
	{
		mRegister.insert(pair<Ogre::MovableObject *, Actor *>(obj, act));
	}
}

Actor * ActorRegister::getActor(Ogre::MovableObject * obj)
{
	map<Ogre::MovableObject *, Actor *>::iterator itr;
	if((itr = mRegister.find(obj)) != mRegister.end())
	{
		return((*itr).second);
	}	
	return(NULL);
}