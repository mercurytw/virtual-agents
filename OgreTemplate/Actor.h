#pragma once
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreVector2.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>
#include <OgreEntity.h>
#include <OgreNode.h>
#include "QuadTreeNode.h"
#include "SceneNode.h"
enum actor_type { agent_t, obstacle_t };
class Actor
{
private:
	int mRadius;
	Ogre::Vector3 mVelocity;
	static int numInstances;
public:
	Ogre::Entity* mDrawable;
	Actor(void); // Delete this
	Actor(Ogre::Entity *drawable, int radius, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent);
	~Actor(void);

	


	Ogre::Vector3 getPosition();
	void setPosition(Ogre::Vector3 newPos);
	Ogre::Quaternion getOrientation();
	void setOrientation(Ogre::Quaternion newOrient);
	void setRotation(Ogre::Real theta);
	Ogre::Real getRotation();
	Ogre::Entity* getDrawable();
	void setDrawable(Ogre::Entity * drawer);
	void setRadius(int radius);
	int getRadius();

	Ogre::Vector3 getDirection();
	void setDirection(Ogre::Vector3 dir);
	void setVelocity(Ogre::Vector3 newVelo);
	Ogre::Vector3 getVelocity();

	actor_type myType;
	SceneNode* mNode;
	int myNum;
};
