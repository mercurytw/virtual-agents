#pragma once
#include <map>
#include <vector>
#include <OgreMesh.h>
#include <OgreManualObject.h>
#include <OgreVector3.h>
#include <OgreString.h>
#include <OgreSceneManager.h>
#include <OgrePlane.h>
#include <OgrePlaneBoundedVolume.h>
#include "Convenience.h"
#include "Nightlogger.h"
using namespace std;

//Singleton Class
class PolytopeFactory
{
private:
	static bool isInitialized;
	static const Ogre::Real height;
	static const Ogre::String MESH_NAME;
	static void createMesh(int numSides);
	PolytopeFactory(void);
	~PolytopeFactory(void);
	static PolytopeFactory *mSingleton;
	static map<int, Ogre::PlaneList *> mPlaneMap;
public:
	static PolytopeFactory* getSingleton();
	pair<Ogre::Entity *, Ogre::PlaneList *> createPolytope(int numSides, Ogre::SceneManager *sm);
};
