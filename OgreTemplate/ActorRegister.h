#pragma once
#include <map>
#include <OgreMovableObject.h>
#include "Actor.h"
#include "Nightlogger.h"
using namespace std;

class ActorRegister
{
private:
	static map<Ogre::MovableObject *, Actor *> mRegister;
	ActorRegister(void);
	~ActorRegister(void);
public:
	
	static void addActor(Ogre::MovableObject * obj, Actor *act);
	static Actor *getActor(Ogre::MovableObject * obj);
};