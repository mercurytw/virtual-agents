{
	"Goals":[[100.0, 100.0],
			 [0.0, 0.0],
			 [0.0, 50.0],
			 [230.0, 0.0],
			 [12.0, 75.0],
			 [12.0, 30.0],
			 [30.0, 30.0],
			 [245.0, 123.0],
			 [115.0, 254.0],
			 [23.0, 12.0],
			 [13.0, 42.0],
			 [100.0, 100.0],
			 [11.0, 0.0],
			 [32.0, 53.0],
			 [111.0, 92.0],
			 [273.0, 16.0],
			 [20.0, 35.0],
			 [30.0, 73.0],
			 [24.0, 123.0]
			],
	
	"Ctlr":{"Type":"Obstacle", "speed":0.05, "direction":[1.0, 0.0, 0.0], "position":[-100.0, 0.0, 10.0],
	        "Obstacle":{"scale":10, "sides":5}},
			
	"Ctlr":{"Type":"Obstacle", "speed":0.05, "direction":[1.0, 0.0, 0.0], "position":[-150.0, 0.0, 40.0],
	        "Obstacle":{"scale":15, "sides":4}},
			
	"Ctlr":{"Type":"Obstacle", "speed":0.2, "direction":[1.0, 0.0, 0.0], "position":[-70.0, 0.0, 40.0],
	        "Obstacle":{"scale":10, "sides":4}},
			
	"Ctlr":{"Type":"Agent", "speed":2.0, "alpha":250.0, "beta":2.0, "gamma":-250.0, "delta":1.0, "A":-250.0, "B":15.0, "C":5.0, "D":1.0,
	        "Agent":{"radius":5, "position":[0.0, 0.0, -20.0]},
			"Goals":["g0", "g1"]
		   },
	
	"Ctlr":{"Type":"Agent", "speed":2.0, "alpha":375.0, "beta":4.0, "gamma":-300.0, "delta":1.0, "A":-375.0, "B":15.0, "C":3.0, "D":1.0,
	        "Agent":{"radius":5, "position":[0.0, 0.0, -50.0]},
			"Goals":["g2", "g3"]
		   },
			
	"Ctlr":{"Type":"Agent", "speed":2.0, "alpha":300.0, "beta":5.0, "gamma":-100.5, "delta":1.0, "A":-300.0, "B":15.0, "C":10.5, "D":1.0,
	        "Agent":{"radius":5, "position":[0.0, 0.0, 20.0]},
			"Goals":["g4", "g5"]
		   },

	"Ctlr":{"Type":"Agent", "speed":2.0, "alpha":300.0, "beta":3.0, "gamma":-150.0, "delta":1.0, "A":-300.0, "B":15.0, "C":15.0, "D":1.0,
	        "Agent":{"radius":5, "position":[-20.0, 0.0, 0.0]},
			"Goals":["g6", "g7"]
		   },
	
	"Ctlr":{"Type":"Agent", "speed":4.0, "alpha":275.0, "beta":2.0, "gamma":-200.0, "delta":1.0, "A":-275.0, "B":15.0, "C":2.0, "D":1.0,
	        "Agent":{"radius":5, "position":[20.0, 0.0, 0.0]},
			"Goals":["g8", "g9"]
		   },
	
	"Ctlr":{"Type":"Agent", "speed":3.0, "alpha":375.0, "beta":4.0, "gamma":-300.0, "delta":1.0, "A":-375.0, "B":15.0, "C":3.0, "D":1.0,
	        "Agent":{"radius":5, "position":[30.0, 0.0, -30.0]},
			"Goals":["g10", "g11"]
		   }
}