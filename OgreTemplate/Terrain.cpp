#include "Terrain.h"

Terrain::Terrain(void)
{

}

Terrain::Terrain(Ogre::SceneManager *sm)
{
	mPlane = new Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);

	Ogre::MeshManager::getSingleton().createPlane("TerrainPlane", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
									*mPlane,
									1500,
									1500,
									 1,
									 1,
									 true,
									 1,
									 1.0,
									 1.0,
									 Ogre::Vector3::UNIT_Z);

	mDrawable = sm->createEntity("TerrainEntity", "TerrainPlane");
	mDrawable->setMaterialName("Examples/GrassFloor");
	mDrawable->setCastShadows(false);
	sm->getRootSceneNode()->createChildSceneNode("TerrainNode")->attachObject(mDrawable);
}

Terrain::~Terrain(void)
{
}
