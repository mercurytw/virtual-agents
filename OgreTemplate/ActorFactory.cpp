#include "ActorFactory.h"
ActorFactory * ActorFactory::mSingleton = NULL;
ActorFactory::ActorFactory(void)
{
}

ActorFactory::~ActorFactory(void)
{
}

ActorFactory* ActorFactory::getSingleton()
{
	if(ActorFactory::mSingleton == NULL)
	{
		ActorFactory::mSingleton = new ActorFactory();
	}
	return(ActorFactory::mSingleton);
}

Actor* ActorFactory::agentFromJSON(rapidjson::Value &json, Ogre::SceneManager *sm, QuadTreeNode *parent)
{
	string caller = "ActorFactory::agentFromJSON";
	Ogre::Vector3 pos;
	Actor *act;

	int radius = (int) Argonaut::getEntry<unsigned int>(json, "radius", caller);
	Argonaut::checkArray(json, "position", 3u, caller);
	Value &pArr = json["position"];
	pos.x = (float)Argonaut::getEntry<float>(pArr[0u], caller);
	pos.y = (float)Argonaut::getEntry<float>(pArr[1u], caller);
	pos.z = (float)Argonaut::getEntry<float>(pArr[2u], caller);

	Ogre::Entity *drawable = CylinderFactory::getSingleton()->createCylinder(sm);

	act = new Actor(drawable, radius, pos, sm, parent);
	act->myType = agent_t;
	ActorRegister::addActor(drawable, act);
	return act;
}

Actor* ActorFactory::obstacleFromJSON(rapidjson::Value &json, Ogre::Vector3 position, Ogre::SceneManager *sm, QuadTreeNode *parent)
{
	string caller = "ActorFactory::obstacleFromJSON";
	Ogre::Vector3 pos;
	Actor *act;

	int numSides = (int) Argonaut::getEntry<unsigned int>(json, "sides", caller);
	int scale = (int) Argonaut::getEntry<unsigned int>(json, "scale", caller);
	pair<Ogre::Entity *, Ogre::PlaneList *> polytope = PolytopeFactory::getSingleton()->createPolytope(numSides, sm);
	Ogre::Entity *drawable = polytope.first;
	
	act = new Actor(drawable, scale, position, sm, parent);
	act->myType = obstacle_t;
	ActorRegister::addActor(drawable, act);
		
	return act;
}
