/*
*  Attention! This source is a modification of the Ogre Tutorial Framework, found here:
*  http://www.ogre3d.org/tikiwiki/Ogre+Wiki+Tutorial+Framework
*  Additional code referenced:
*  http://www.ogre3d.org/tikiwiki/Intermediate+Tutorial+5&structure=Tutorials
*/
/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _ 
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/                              
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
#include <list>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include <OgrePlane.h>
#include <OgrePlaneBoundedVolume.h>
#include "World.h"
#include "Nightlogger.h"
#include "Argonaut.h"
#include "Actor.h"
#include "QuadTreeNode.h"
#include "INode.h"
#include "SceneNode.h"
#include "AgentController.h"
#include "ControllerFactory.h"
#include "PolytopeFactory.h"
#include "Convenience.h"
#include "GoalRegister.h"
using namespace std;
using namespace rapidjson;

rapidjson::Document json_document;

#ifdef _DEBUG
#define LOGFILE "debugLog.txt"
#else
#define NDEBUG
#define LOGFILE "log.txt"
#endif


list<IController *> controllers;
QuadTreeNode* root;
//-------------------------------------------------------------------------------------
World::World(void)
{
}
//-------------------------------------------------------------------------------------
World::~World(void)
{
}

void World::makePlane()
{
	Ogre::Plane *plane = new Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);

	Ogre::MeshManager::getSingleton().createPlane("TerrainPlane", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
									*plane,
									1500,
									1500,
									 1,
									 1,
									 true,
									 1,
									 1.0,
									 1.0,
									 Ogre::Vector3::UNIT_Z);

	Ogre::Entity *ent = mSceneMgr->createEntity("TerrainEntity", "TerrainPlane");
	ent->setMaterialName("Examples/GrassFloor");
	ent->setCastShadows(false);
	mSceneMgr->getRootSceneNode()->createChildSceneNode("TerrainNode")->attachObject(ent);
}

//-------------------------------------------------------------------------------------
void World::createScene(void)
{
	mCamera->setPosition(0.0, 20.0, 0.0);
	mSceneMgr->setAmbientLight(Ogre::ColourValue(1.0, 1.0, 1.0));

	makePlane();
	
	root = new QuadTreeNode();

	FILE *json_input_file = 0;
	fopen_s(&json_input_file, "SceneData.txt", "r");
	if( json_input_file != 0 )
	{
		FileStream s(json_input_file);
		if( json_document.ParseStream<0>(s).HasParseError() )
		{
			Nightlogger::getSingleton()->writeLog(log_error, "main::createScene: failed to parse json\n", cerr);
			assert(false);
		}
		fclose(json_input_file);


		Argonaut::checkJSONForm(json_document.IsObject(), "main::createScene");

		Nightlogger::getSingleton()->writeLog(log_comment, "\n/==============\nReading Game Input\n/==============");
		Value::ConstMemberIterator itr;
		for(itr = json_document.MemberBegin(); itr != json_document.MemberEnd(); itr++)
		{
			string name = string(itr->name.GetString());
			Value &jsonOb = (Value &)itr->value;

			if(name.compare("Ctlr") == 0)
			{
				Argonaut::checkJSONForm(jsonOb.IsObject(), "main::createScene");
				IController *nAge = ControllerFactory::getSingleton()->controllerFromJSON(jsonOb, mSceneMgr, root);
				controllers.push_back(nAge);
			}
			else if(name.compare("Goals") == 0)
			{
				Argonaut::checkJSONForm(jsonOb.IsArray(), "main::createScene");
				GoalRegister::getSingleton()->buildRegisterFromJSON(jsonOb);
			}
		}

		mSceneMgr->setSkyBox(true, "Examples/CloudyNoonSkyBox", 5000, true, Ogre::Quaternion::IDENTITY, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		Nightlogger::getSingleton()->writeLog(log_comment, "\n/==============\nStarting Simulation\n/==============");
		return;
	}
	Nightlogger::getSingleton()->writeLog(log_error, "main::createScene: failed to open SceneData.txt");
	
	exit(EXIT_FAILURE);
}
//-------------------------------------------------------------------------------------
bool World::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	list<IController *>::iterator cit;
	for(cit = controllers.begin(); cit != controllers.end(); cit++)
	{
		(*cit)->tick(evt.timeSinceLastFrame);
	}
    bool ret = BaseApplication::frameRenderingQueued(evt);
    return ret;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        World app;
		Nightlogger::getSingleton()->openLogfile(LOGFILE);
        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif
