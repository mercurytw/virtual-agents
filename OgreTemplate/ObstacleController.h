#pragma once
#include <stdlib.h>
#include <cmath>
#include <OgreVector3.h>
#include "IController.h"
#include "Actor.h"
#include "INode.h"
#include "QuadTreeNode.h"
#include "SceneNode.h"

class ObstacleController: public IController
{
private:
	Ogre::Vector3 mDirection;
	float mSpeed;
	Actor *mObs;
public:
	ObstacleController(Actor *obstacle, float speed, Ogre::Vector3 direction);
	~ObstacleController(void);

	void tick(float deltaTime);
	void deinitialize() {return;}
};
