#include "Agent.h"

int Agent::numInstances = 0;
Agent::Agent(int radius, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	mRadius = radius;
	mDrawable = CylinderFactory::getSingleton()->createCylinder(sm);
	Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode(position);
	node->attachObject(mDrawable);
	node->scale(Ogre::Real(mRadius), 1.0f, Ogre::Real(mRadius));
	mVelocity = Ogre::Vector3(0.0f, 0.0f, 0.0f);
	mNode = new SceneNode(parent, this);
	parent->sceneChildren.push_back(mNode);
	myNum = numInstances++;
	myType = agent_t;
}

Agent::Agent(Value &json, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	string caller = "Agent::Agent";
	Ogre::Vector3 pos;

	mRadius = (int) Argonaut::getEntry<unsigned int>(json, "radius", caller);
	Argonaut::checkArray(json, "position", 3u, caller);
	Value &pArr = json["position"];
	pos.x = (float)Argonaut::getEntry<float>(pArr[0u], caller);
	pos.y = (float)Argonaut::getEntry<float>(pArr[1u], caller);
	pos.z = (float)Argonaut::getEntry<float>(pArr[2u], caller);

	mDrawable = CylinderFactory::getSingleton()->createCylinder(sm);
	ActorRegister::addActor(mDrawable, this);
	Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode(pos);
	node->attachObject(mDrawable);
	node->scale(Ogre::Real(mRadius), 1.0f, Ogre::Real(mRadius));
	mVelocity = Ogre::Vector3(0.0f, 0.0f, 0.0f);
	mNode = new SceneNode(parent, this);
	parent->sceneChildren.push_back(mNode);
	myNum = numInstances++;
	myType = agent_t;
}

Agent::~Agent(void)
{
}

Ogre::Vector3 Agent::getDirection()
{
	return(this->getOrientation().zAxis());//.reflect(this->getOrientation().zAxis()));
}

void Agent::setDirection(Ogre::Vector3 dir)
{	
	this->setOrientation(Ogre::Vector3::UNIT_Z.getRotationTo(dir));
}

void Agent::setVelocity(Ogre::Vector3 newVelo)
{
	mVelocity = newVelo;
}

Ogre::Vector3 Agent::getVelocity()
{
	return(mVelocity);
}

int Agent::getRadius()
{
	return(mRadius);
}
