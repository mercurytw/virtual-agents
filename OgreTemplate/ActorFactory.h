#pragma once
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreVector3.h>
#include <OgrePlane.h>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include "CylinderFactory.h"
#include "QuadTreeNode.h"
#include "Argonaut.h"
#include "ActorRegister.h"
#include "PolytopeFactory.h"
using namespace std;
using namespace rapidjson;

//Singleton Class
class ActorFactory
{
private:
	ActorFactory(void);
	~ActorFactory(void);
	static ActorFactory* mSingleton;
public:
	static ActorFactory* getSingleton();
	Actor* agentFromJSON(Value &json, Ogre::SceneManager* sm, QuadTreeNode* parent);
	Actor* obstacleFromJSON(Value &json, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent);
};
