#pragma once
#include <OgreMesh.h>
#include <OgreManualObject.h>
#include <OgreVector3.h>
#include <OgreString.h>
#include <OgreSceneManager.h>

//Singleton Class
class CylinderFactory
{
private:
	static bool isInitialized;
	static const int numVerticies;
	static const Ogre::Real height;
	static const Ogre::String MESH_NAME;
	static void createMesh();
	CylinderFactory(void);
	~CylinderFactory(void);
	static CylinderFactory* singleton;
public:
	static CylinderFactory* getSingleton();
	Ogre::Entity *createCylinder(Ogre::SceneManager *sm);
};
