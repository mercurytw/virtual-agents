#pragma once
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreVector3.h>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include "Actor.h"
#include "Argonaut.h"
#include "CylinderFactory.h"
#include "QuadTreeNode.h"
#include "SceneNode.h"
#include "ActorRegister.h"
using namespace std;
using namespace rapidjson;

class Agent: public Actor
{
private:
	Ogre::Vector3 mVelocity;
	int mRadius;
	static int numInstances;
public:
	Agent(int radius, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent);
	Agent(Value &json, Ogre::SceneManager* sm, QuadTreeNode* parent);
	~Agent(void);

	Ogre::Vector3 getDirection();
	void setDirection(Ogre::Vector3 dir);
	void setVelocity(Ogre::Vector3 newVelo);
	Ogre::Vector3 getVelocity();
	int getRadius();
	SceneNode* mNode;
	int myNum;
};
