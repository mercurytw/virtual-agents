#include "ControllerFactory.h"

ControllerFactory * ControllerFactory::mSingleton = NULL;
ControllerFactory::ControllerFactory(void)
{
	mController = NULL;
}

ControllerFactory::~ControllerFactory(void)
{
	delete mSingleton;
}

ControllerFactory* ControllerFactory::getSingleton()
{
	if(mSingleton == NULL)
	{
		mSingleton = new ControllerFactory();
	}
	return(mSingleton);
}

IController *ControllerFactory::controllerFromJSON(Value &json, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	IController *newCtlr = NULL;
	string caller = "ControllerFactory::controllerFromJSON";
	string *type = Argonaut::getEntry<string *>(json, "Type", caller);
	if(type->compare("Agent") == 0)
	{
		float speed, alpha, beta, gamma, delta, A, B, C, D;
		speed = (float) Argonaut::getEntry<float>(json, "speed", caller);
		alpha = (float) Argonaut::getEntry<float>(json, "alpha", caller);
		beta = (float) Argonaut::getEntry<float>(json, "beta", caller);
		gamma = (float) Argonaut::getEntry<float>(json, "gamma", caller);
		delta = (float) Argonaut::getEntry<float>(json, "delta", caller);
		A = (float) Argonaut::getEntry<float>(json, "A", caller);
		B = (float) Argonaut::getEntry<float>(json, "B", caller);
		C = (float) Argonaut::getEntry<float>(json, "C", caller);
		D = (float) Argonaut::getEntry<float>(json, "D", caller);

		Argonaut::checkObject(json, "Agent", caller);
		Value &ajson = json["Agent"];
		Actor *act = ActorFactory::getSingleton()->agentFromJSON(ajson, sm, parent);
		newCtlr = new AgentController(act, sm, speed, alpha, beta, gamma, delta, A, B, C, D);

		Argonaut::checkArray(json, "Goals", caller);
		Value &gjson = json["Goals"];
		
		unsigned int arrSiz = gjson.Size();
		string *goalName;
		for(unsigned int i = 0u; i < arrSiz; i++)
		{
			Value &goal = gjson[i];
			goalName = (string *)Argonaut::getEntry<string *>(goal, caller);
			((AgentController *)newCtlr)->addGoal(goalName);
		}
	}
	else if(type->compare("Obstacle") == 0)
	{
		float speed;
		Ogre::Vector3 dir, pos;
		speed = (float) Argonaut::getEntry<float>(json, "speed", caller);
		Argonaut::checkArray(json, "direction", 3u, caller);
		Value &dArr = json["direction"];
		dir.x = (float)Argonaut::getEntry<float>(dArr[0u], caller);
		dir.y = (float)Argonaut::getEntry<float>(dArr[1u], caller);
		dir.z = (float)Argonaut::getEntry<float>(dArr[2u], caller);
		
		Argonaut::checkArray(json, "position", 3u, caller);
		dArr = json["position"];
		pos.x = (float)Argonaut::getEntry<float>(dArr[0u], caller);
		pos.y = (float)Argonaut::getEntry<float>(dArr[1u], caller);
		pos.z = (float)Argonaut::getEntry<float>(dArr[2u], caller);
		
		Argonaut::checkObject(json, "Obstacle", caller);
		Value &ojson = json["Obstacle"];
		Actor *act = ActorFactory::getSingleton()->obstacleFromJSON(ojson, pos, sm, parent);
		newCtlr = new ObstacleController(act, speed, dir);
	}
	delete type;
	return(newCtlr);
}