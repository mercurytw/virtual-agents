#include "ObstacleController.h"

ObstacleController::ObstacleController(Actor *obstacle, float speed, Ogre::Vector3 direction)
{
	mObs = obstacle;
	mSpeed = speed;
	mDirection = direction;
}

ObstacleController::~ObstacleController(void)
{
}

void ObstacleController::tick(float deltaTime)
{
	Ogre::Vector3 oldPos = mObs->getPosition();
	Ogre::Vector3 newPos = oldPos + (mDirection * mSpeed);
	mObs->setPosition(newPos);
}
