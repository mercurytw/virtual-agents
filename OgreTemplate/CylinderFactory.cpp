#include "CylinderFactory.h"

bool CylinderFactory::isInitialized = false;
const int CylinderFactory::numVerticies = 40;
const Ogre::Real CylinderFactory::height = 10.0f;
const Ogre::String CylinderFactory::MESH_NAME = "CylinderFactoryMesh";
CylinderFactory* CylinderFactory::singleton = NULL;
CylinderFactory::CylinderFactory(void)
{
}

CylinderFactory::~CylinderFactory(void)
{
	delete singleton;
}

void CylinderFactory::createMesh(void)
{
	Ogre::ColourValue CylinderFactoryColor(0.0f, 0.0f, 1.0f);
	float step = (2.0f * Ogre::Math::PI) / CylinderFactory::numVerticies;
	Ogre::Vector3 point(0.0f, CylinderFactory::height*2.0f, 1.0f);
	Ogre::ManualObject cylOb("CylinderFactoryObject");

	//Top face
	cylOb.begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_FAN);
	{
		cylOb.position(0.0f, CylinderFactory::height*2.0f, 0.0f);
		cylOb.colour(CylinderFactoryColor);
		
		for(float i = step; i <= 2 *Ogre::Math::PI; i += step)
		{
			point.z = Ogre::Math::Cos(Ogre::Radian(i));
			point.x = Ogre::Math::Sin(Ogre::Radian(i));
			cylOb.position(point);
			cylOb.colour(CylinderFactoryColor);
		}
		cylOb.position(point);
		cylOb.colour(CylinderFactoryColor);

		cylOb.position(0.0f, CylinderFactory::height*2.0f, 1.0f);
		cylOb.colour(CylinderFactoryColor);

		cylOb.position(Ogre::Math::Sin(step), CylinderFactory::height*2.0f, Ogre::Math::Cos(step));
		cylOb.colour(CylinderFactoryColor);
	}
	cylOb.end();

	//direction
	cylOb.begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_FAN);
	{
		float ht = (CylinderFactory::height*2.0f) + 0.1f;
		Ogre::Vector3 newpt(0.0f, ht, -1.0f);
		cylOb.position(0.0f, ht, 0.0f);
		cylOb.colour(1.0f, 1.0f, 1.0f);

		cylOb.position(Ogre::Math::Sin(Ogre::Radian(Ogre::Math::PI - step)), ht, Ogre::Math::Cos(Ogre::Radian(Ogre::Math::PI -step)));
		//cylOb.position(point);
		cylOb.colour(1.0f, 1.0f, 1.0f);

		//cylOb.position(0.0f, CylinderFactory::height*2.0f, 1.0f);
		cylOb.position(newpt);
		cylOb.colour(1.0f, 1.0f, 1.0f);

		cylOb.position(Ogre::Math::Sin(Ogre::Radian(Ogre::Math::PI + step)), ht, Ogre::Math::Cos(Ogre::Radian(Ogre::Math::PI + step)));
		cylOb.colour(1.0f, 1.0f, 1.0f);
	}
	cylOb.end();

	point.x = 0.0f;
	point.z = 1.0f;
	//Side
	cylOb.begin("BaseWhiteNoLighting",Ogre::RenderOperation::OT_TRIANGLE_STRIP);
	{
		cylOb.position(point.x, CylinderFactory::height*2.0f, point.z);
		cylOb.colour(CylinderFactoryColor);

		cylOb.position(point.x, 0.0f, point.z);
		cylOb.colour(CylinderFactoryColor);
		for(float i = 0.0f; i <=  2.0f * Ogre::Math::PI; i += step)
		{
			point.z = Ogre::Math::Cos(Ogre::Radian(i));
			point.x = Ogre::Math::Sin(Ogre::Radian(i));
			cylOb.position(point.x, CylinderFactory::height*2.0f, point.z);
			cylOb.colour(CylinderFactoryColor);

			cylOb.position(point.x, 0.0f, point.z);
			cylOb.colour(CylinderFactoryColor);
		}
		cylOb.position(0.0f, CylinderFactory::height*2.0f, 1.0f);
		cylOb.colour(CylinderFactoryColor);

		cylOb.position(0.0f, 0.0f, 1.0f);
		cylOb.colour(CylinderFactoryColor);
	}
	cylOb.end();
	cylOb.convertToMesh(CylinderFactory::MESH_NAME);
}


CylinderFactory* CylinderFactory::getSingleton()
{
	if(!isInitialized)
	{
		CylinderFactory::createMesh();
		singleton = new CylinderFactory();
	}
	return(singleton);
}

Ogre::Entity* CylinderFactory::createCylinder(Ogre::SceneManager *sm)
{
	return(sm->createEntity(CylinderFactory::MESH_NAME));
}
