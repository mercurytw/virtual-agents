#pragma once
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreVector3.h>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include "Argonaut.h"
#include "AgentController.h"
#include "ActorFactory.h"
#include "ObstacleController.h"
#include "QuadTreeNode.h"
using namespace rapidjson;

//Singleton Class
class ControllerFactory
{
private:
	ControllerFactory();
	~ControllerFactory();
	static ControllerFactory* mSingleton;
	IController *mController;
public:
	static ControllerFactory* getSingleton();
	IController* controllerFromJSON(Value &json, Ogre::SceneManager* sm, QuadTreeNode* parent);
};
