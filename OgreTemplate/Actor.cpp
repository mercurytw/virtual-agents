#include "Actor.h"

int Actor::numInstances = 0;
Actor::Actor(void)
{
}

Actor::Actor(Ogre::Entity *drawable, int radius, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	mRadius = radius;
	mDrawable = drawable;
	Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode(position);
	node->attachObject(mDrawable);
	node->scale(Ogre::Real(mRadius), 1.0f, Ogre::Real(mRadius));
	mVelocity = Ogre::Vector3(0.0f, 0.0f, 0.0f);
	mNode = new SceneNode(parent, this);
	parent->sceneChildren.push_back(mNode);
	myNum = numInstances++;
}

Actor::~Actor(void)
{
}



Ogre::Vector3 Actor::getPosition()
{
	return(mDrawable->getParentNode()->getPosition());
}

void Actor::setPosition(Ogre::Vector3 newPos)
{
	mDrawable->getParentNode()->setPosition(newPos);
}

Ogre::Quaternion Actor::getOrientation()
{
	return(mDrawable->getParentNode()->getOrientation());
}
void Actor::setOrientation(Ogre::Quaternion newOrient)
{
	mDrawable->getParentNode()->resetOrientation();
	mDrawable->getParentNode()->setOrientation(newOrient);
}

void Actor::setRotation(Ogre::Real theta)
{
	Ogre::Quaternion rot;
	mDrawable->getParentNode()->resetOrientation();
	rot.FromAngleAxis(Ogre::Radian(Ogre::Math::DegreesToRadians(Ogre::Real(theta))), Ogre::Vector3::UNIT_Y);
	mDrawable->getParentNode()->setOrientation(rot);
}

Ogre::Real Actor::getRotation()
{
	return(mDrawable->getParentNode()->getOrientation().getYaw().valueDegrees());
}

void Actor::setDrawable(Ogre::Entity* drawer)
{
	mDrawable = drawer;
}

Ogre::Entity *Actor::getDrawable()
{
	return(mDrawable);
}

void Actor::setRadius(int radius)
{
	mRadius = radius;
}

int Actor::getRadius()
{
	return mRadius;
}

Ogre::Vector3 Actor::getDirection()
{
	return(this->getOrientation().zAxis());//.reflect(this->getOrientation().zAxis()));
}

void Actor::setDirection(Ogre::Vector3 dir)
{	
	this->setOrientation(Ogre::Vector3::UNIT_Z.getRotationTo(dir));
}

void Actor::setVelocity(Ogre::Vector3 newVelo)
{
	mVelocity = newVelo;
}

Ogre::Vector3 Actor::getVelocity()
{
	return(mVelocity);
}