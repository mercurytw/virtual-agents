#pragma once
#include <stdlib.h>
#include <cmath>
#include <list>
#include <OgreVector2.h>
#include <OgreVector3.h>
#include <OgreSphere.h>
#include <OgreSceneManager.h>
#include <OgreSceneQuery.h>
#include <OgreRay.h>
#include "IController.h"
#include "Actor.h"
#include "INode.h"
#include "QuadTreeNode.h"
#include "SceneNode.h"
#include "ActorRegister.h"
#include "GoalRegister.h"
using namespace std;

class AgentController: public IController
{
private:
	bool mHasFirstGoal;
	bool mIsTorndown;
	Actor *myAgent;
	float alpha, beta, gamma, delta, A, B, C, D, v_d;
	Ogre::Vector3 e;
	list<string *> mGoals;
	Ogre::Vector3 simpleCollide();
	Ogre::Vector3 soc(Actor *aj, int rij, Ogre::Vector3 dij, Ogre::Vector3 nij);
	Ogre::Vector3 att(Actor *aj, int rij, Ogre::Vector3 dij, Ogre::Vector3 nij);
	Ogre::Vector3 obs(Actor *ok, int ri, Ogre::Vector3 dik, Ogre::Vector3 nik);
	Ogre::Vector3 obsat(Actor *ok, int ri, Ogre::Vector3 dik, Ogre::Vector3 nik);
	Ogre::Vector3 fric(float r_ij, Ogre::Vector3 d_ij, Ogre::Vector3 n_ij, Ogre::Vector3 t_ij);
	Ogre::Vector3 push(float r_ij, Ogre::Vector3 d_ij, Ogre::Vector3 n_ij);
	void getAgentForce(Actor *other, Ogre::Vector3 *result);
	void getObstacleForce(Actor *other, Ogre::Vector3 *result);
	Ogre::Vector3 mOldpos; //DELETE THIS!
	Ogre::RaySceneQuery *mRayQuery;
	Ogre::SceneManager *mSceneMgr;
public:
	AgentController(Actor *ag, Ogre::SceneManager *sm, float desiredSpeed, float a, float b, float g, float d, float aye, float bee, float sea, float dee);
	~AgentController(void);

	static float tau;
	static float kappa;
	static float lambda;
	static float t_p;

	void tick(float deltaTime);
	Ogre::Vector3 getForces();
	void deinitialize();
	void addGoal(string *goal);
};
