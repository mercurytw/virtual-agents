#include "Obstacle.h"

int Obstacle::numInstances = 0;
Obstacle::Obstacle(int scale, int numSides, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	mNumSides = numSides;
	int mScale = scale;
	mDrawable = PolytopeFactory::getSingleton()->createPolytope(numSides, sm).first;
	Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode(position);
	node->attachObject(mDrawable);
	//node->rotate(Ogre::Vector3::UNIT_Y, Ogre::Radian(Ogre::Degree((Ogre::Real) 45.0f)));
	node->scale(Ogre::Real(mScale), 1.0f, Ogre::Real(mScale));
	mNode = new SceneNode(parent, this);
	parent->sceneChildren.push_back(mNode);
	myType = obstacle_t;
	myNum = numInstances++;

	//mSceneMgr = sm;
}

Obstacle::Obstacle(Value &json, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent)
{
	string caller = "Obstacle::Obstacle";
	mNumSides = (int) Argonaut::getEntry<unsigned int>(json, "sides", caller);
	int mScale = (int) Argonaut::getEntry<unsigned int>(json, "scale", caller);
	pair<Ogre::Entity *, Ogre::PlaneList *> polytope = PolytopeFactory::getSingleton()->createPolytope(mNumSides, sm);
	mDrawable = polytope.first;
	ActorRegister::addActor(mDrawable, this);
		
	Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode(position);
	node->attachObject(mDrawable);
	//node->rotate(Ogre::Vector3::UNIT_Y, Ogre::Radian(Ogre::Degree((Ogre::Real) 45.0f)));
	node->scale(Ogre::Real(mScale), 1.0f, Ogre::Real(mScale));
	mNode = new SceneNode(parent, this);
	parent->sceneChildren.push_back(mNode);
	myType = obstacle_t;
	myNum = numInstances++;

	//mSceneMgr = sm;


}

Obstacle::~Obstacle(void)
{
}
