#include "PolytopeFactory.h"

bool PolytopeFactory::isInitialized = false;
const Ogre::Real PolytopeFactory::height = 11.0f;
const Ogre::String PolytopeFactory::MESH_NAME = "PolytopeFactoryMesh";
PolytopeFactory* PolytopeFactory::mSingleton = NULL;
map<int, Ogre::PlaneList *> PolytopeFactory::mPlaneMap;
PolytopeFactory::PolytopeFactory(void)
{
}

PolytopeFactory::~PolytopeFactory(void)
{
	map<int, Ogre::PlaneList *>::iterator iit;
	for(iit = mPlaneMap.begin(); iit != mPlaneMap.end(); iit++) // if we haven't made a polytope with this many sides yet...
	{
		(*iit).second->clear();
		delete (*iit).second;
	}
	mPlaneMap.clear();
	delete mSingleton;
}

void PolytopeFactory::createMesh(int numSides)
{
	Ogre::Vector3 backupPoint;
	Ogre::PlaneList *newList = new Ogre::PlaneList;
	vector<Ogre::Vector3> points;
	float step = (2.0f * Ogre::Math::PI) / numSides;
	float addit = step + (Ogre::Math::PI/4.0f);
	Ogre::ColourValue PolytopeFactoryColor(0.0f, 1.0f, 0.0f);
	Ogre::Vector3 point(0.0f, PolytopeFactory::height*2.0f, 0.1f);
	Ogre::ManualObject polyOb("PolytopeFactoryObject" + arbToStr<int>(numSides));

	newList->push_back(Ogre::Plane(Ogre::Vector3::UNIT_Y, Ogre::Vector3((Ogre::Real) 0.0f, PolytopeFactory::height*2.0f, (Ogre::Real) 0.0f)));
	newList->push_back(Ogre::Plane(Ogre::Vector3::NEGATIVE_UNIT_Y, Ogre::Vector3((Ogre::Real) 0.0f, -PolytopeFactory::height*2.0f, (Ogre::Real) 0.0f)));

	//Top face
	polyOb.begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_FAN);
	{
		polyOb.position(0.0f, PolytopeFactory::height*2.0f, 0.0f);
		polyOb.colour(PolytopeFactoryColor);
		
		for(float i = step - (Ogre::Math::PI/4); i <= (2 *Ogre::Math::PI) + addit; i += step)
		{
			point.z = Ogre::Math::Cos(Ogre::Radian(i));
			point.x = Ogre::Math::Sin(Ogre::Radian(i));
			polyOb.position(point);
			polyOb.colour(PolytopeFactoryColor);
		}
	}
	polyOb.end();

	point.x = Ogre::Math::Sin(step  - (Ogre::Math::PI/4));
	point.z = Ogre::Math::Cos(step  - (Ogre::Math::PI/4));
	//Side
	polyOb.begin("BaseWhiteNoLighting",Ogre::RenderOperation::OT_TRIANGLE_STRIP);
	{
		for(float i = step - (Ogre::Math::PI/4); i <= (2 *Ogre::Math::PI) + addit; i += step)
		{
			point.z = Ogre::Math::Cos(Ogre::Radian(i));
			point.x = Ogre::Math::Sin(Ogre::Radian(i));
			polyOb.position(point.x, PolytopeFactory::height*2.0f, point.z);
			polyOb.colour(PolytopeFactoryColor);

			points.push_back(Ogre::Vector3(point.x, PolytopeFactory::height*2.0f, point.z));

			if( points.size() == 3 )
			{
				newList->push_back(Ogre::Plane(points[0], points[1], points[2]));
				backupPoint = points[2];
				points.clear();
				points.push_back(backupPoint);
			}

			polyOb.position(point.x, 0.0f, point.z);
			polyOb.colour(PolytopeFactoryColor);

			points.push_back(Ogre::Vector3(point.x, 0.0f, point.z));
		}
	}
	polyOb.end();
	polyOb.convertToMesh(PolytopeFactory::MESH_NAME + arbToStr<int>(numSides));
	mPlaneMap.insert(pair<int, Ogre::PlaneList *>(numSides, newList));
}

PolytopeFactory* PolytopeFactory::getSingleton()
{
	if(!isInitialized)
	{
		mSingleton = new PolytopeFactory();
	}
	return(mSingleton);
}

pair<Ogre::Entity *, Ogre::PlaneList *> PolytopeFactory::createPolytope(int numSides, Ogre::SceneManager *sm)
{
	// Polytopes of size 3 will have their "centers" on a face which will throw off collision
	// any less, and we start running into issues such as the polytope not having volume...
	if(numSides < 4) 
	{
		Nightlogger::getSingleton()->writeLog(log_warning, "PolyFact::createPolytope: Encountered a request for a polytope w/ less than 4 side faces. This is not acceptable. Returning NULL!");
		return(pair<Ogre::Entity *, Ogre::PlaneList *>(NULL, NULL));
	}
	map<int, Ogre::PlaneList *>::iterator iit;
	if((iit = mPlaneMap.find(numSides)) == mPlaneMap.end()) // if we haven't made a polytope with this many sides yet...
	{
		createMesh(numSides); // then make one!
		iit = mPlaneMap.find(numSides); // now we have one, so we can do this.
	}
	Ogre::Entity *newEnt = sm->createEntity(PolytopeFactory::MESH_NAME + arbToStr<int>(numSides));
	return(pair<Ogre::Entity *, Ogre::PlaneList *>(newEnt, (*iit).second));
}
