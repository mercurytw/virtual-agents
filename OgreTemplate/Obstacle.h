#pragma once
#include <vector>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreVector3.h>
#include <OgrePlane.h>
#include <OgreMovablePlane.h>
#include <OgrePlaneBoundedVolume.h>
#include <OgreRay.h>
#include <OgreSphere.h>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include "Actor.h"
#include "Argonaut.h"
#include "PolytopeFactory.h"
#include "QuadTreeNode.h"
#include "SceneNode.h"
#include "Convenience.h"
#include "Nightlogger.h"
#include "ActorRegister.h"
using namespace std;
using namespace rapidjson;

class Obstacle: public Actor
{
private:
	static int numInstances;
	int mNumSides;
	//int mScale;
	//Ogre::SceneManager *mSceneMgr;
public:
	Obstacle(int scale, int numSides, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent);
	Obstacle(Value &json, Ogre::Vector3 position, Ogre::SceneManager* sm, QuadTreeNode* parent);
	~Obstacle(void);

	SceneNode* mNode;
	int myNum;
};
