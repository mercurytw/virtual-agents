#pragma once
#include <OgreRoot.h>
#include <OgreMeshManager.h>
#include <OgreSceneManager.h>
#include "Actor.h"


class Terrain : public Actor
{
public:
	Ogre::Plane* mPlane;
	Terrain(void);
	Terrain(Ogre::SceneManager *sm);
	~Terrain(void);
};
