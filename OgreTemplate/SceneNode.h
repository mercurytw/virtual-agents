#pragma once
#include "INode.h"

class SceneNode : public INode
{
public:
	SceneNode(INode *parent, void* act);
	~SceneNode(void);

	INode* parentQuadTreeNode;
	void* content;
};
