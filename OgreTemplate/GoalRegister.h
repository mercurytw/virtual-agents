#pragma once
#include <map>
#include <OgreVector2.h>
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/rapidjson.h"
#include "Argonaut.h"
#include "Convenience.h"
using namespace std;
using namespace rapidjson;

//Singleton Class
class GoalRegister
{
private:
	map<string *, Ogre::Vector2> mRegister;
	GoalRegister(void);
	~GoalRegister(void);
	static GoalRegister *mSingleton;
public:
	
	void addGoal(string name, Ogre::Vector2 pos);
	Ogre::Vector2 getGoal(string name);
	static GoalRegister *getSingleton(void);
	void buildRegisterFromJSON(Value &json);
};