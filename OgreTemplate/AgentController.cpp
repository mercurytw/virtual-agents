#include "AgentController.h"

float AgentController::tau = 3.0f;
float AgentController::kappa = 0.5f;
float AgentController::lambda = 1.0f;
float AgentController::t_p = 5.0f;
AgentController::AgentController(Actor *ag, Ogre::SceneManager *sm, float desiredSpeed, 
								 float a, float b, float g, float d, 
								 float aye, float bee, float sea, float dee)
{
	mHasFirstGoal = false;
	mIsTorndown = false;
	myAgent = ag;
	alpha = a;
	beta = b;
	gamma = g;
	delta = d;
	A = aye;
	B = bee;
	C = sea;
	D = dee;
	e = Ogre::Vector3::ZERO;
	v_d = desiredSpeed;
	mSceneMgr = sm;
	mRayQuery = mSceneMgr->createRayQuery(Ogre::Ray());

	/*e.x = (rand() % 100) * 1.0f;
	e.z = (rand() % 100) * 1.0f;*/
	
}

AgentController::~AgentController(void)
{
	deinitialize();
}

// the goal field should be new'd
void AgentController::addGoal(string *goal)
{
	mGoals.push_back(goal);
}

void AgentController::deinitialize(void)
{
	if(!mIsTorndown)
	{
		mIsTorndown = true;
		list<string *>::iterator itr;
		for(itr = mGoals.begin(); itr != mGoals.end(); itr++)
		{
			delete (*itr);
		}
	}
}

Ogre::Vector3 AgentController::soc(Actor *aj, int rij, Ogre::Vector3 dij, Ogre::Vector3 nij)
{
	return(alpha*exp((rij - dij.length())/beta)*nij);
}

Ogre::Vector3 AgentController::att(Actor *aj, int rij, Ogre::Vector3 dij, Ogre::Vector3 nij)
{
	return(gamma*exp((rij - dij.length())/delta)*nij);
}

Ogre::Vector3 AgentController::obs(Actor *ok, int ri, Ogre::Vector3 dik, Ogre::Vector3 nik)
{
	return(A*exp((ri - dik.length())/B)*nik);
}

Ogre::Vector3 AgentController::obsat(Actor *ok, int ri, Ogre::Vector3 dik, Ogre::Vector3 nik)
{
	return(C*exp((ri - dik.length())/D)*nik);
}

Ogre::Vector3 AgentController::push(float r_ij, Ogre::Vector3 d_ij, Ogre::Vector3 n_ij)
{
	return(kappa*(r_ij - d_ij.length())*n_ij);
}

Ogre::Vector3 AgentController::fric(float r_ij, Ogre::Vector3 d_ij, Ogre::Vector3 n_ij, Ogre::Vector3 t_ij)
{
	return(lambda*push(r_ij, d_ij, n_ij).length()*t_ij);
	
}

Ogre::Vector3 AgentController::simpleCollide()
{
	list<INode *> *testa = &(((QuadTreeNode *)(myAgent->mNode->parentQuadTreeNode))->sceneChildren);

	list<INode *>::iterator it;
	for(it = testa->begin(); it != testa->end(); it++)
	{
		SceneNode * blah = ((SceneNode *)(*it));
		if(((Actor *)(blah->content))->myType == agent_t)
		{
			Actor *other = ((Actor *)(blah->content));
			if(other->myNum != myAgent->myNum)
			{
				float d_ij, r_ij, real_rij;
				real_rij = (other->getRadius() + myAgent->getRadius()) * 1.0f;
				r_ij = real_rij * real_rij;
				d_ij = myAgent->getPosition().squaredDistance(other->getPosition());
				if(d_ij <= r_ij)
				{
					Ogre::Vector3 moveAway = myAgent->getPosition() - other->getPosition();
					float amt = real_rij - myAgent->getPosition().distance(other->getPosition());
					myAgent->setPosition(myAgent->getPosition() + (moveAway.normalisedCopy() * (amt)));
					Ogre::Vector3 norm = -moveAway;
					Ogre::Vector3 t_ij = norm.crossProduct(Ogre::Vector3::UNIT_Y);
					return(fric(real_rij, norm, norm.normalisedCopy(), t_ij));
				}
			}
		}
		else // it's an obstacle.
		{
			Actor *other = ((Actor *)(blah->content));
			Ogre::Vector3 dir = other->getPosition() - myAgent->getPosition();
			dir.normalise();
			Ogre::Ray movement(myAgent->getPosition(), dir);
			// from http://www.ogre3d.org/tikiwiki/Intermediate+Tutorial+2&structure=Tutorials
			mRayQuery->setRay(movement);
			Ogre::RaySceneQueryResult &res = mRayQuery->execute();
			Ogre::RaySceneQueryResult::iterator itr = res.begin();
			bool foundNearestObstacle = false;
			for(; (foundNearestObstacle == false) && (itr != res.end()); itr++)
			{
				Actor * hit = ActorRegister::getActor(itr->movable);
				if(hit && (hit->myType != this->myAgent->myType))
				{
					foundNearestObstacle = true;
					Ogre::Real amt = itr->distance;
					amt = ((Ogre::Real)myAgent->getRadius()) - amt;
					if(amt > (Ogre::Real) 0.0f)
					{
						dir = dir * -1.0f;
						myAgent->setPosition(myAgent->getPosition() + (dir * amt));
						float r_ij = (float)myAgent->getRadius();
						dir = dir * -1.0f;
						Ogre::Vector3 t_ij = dir.crossProduct(Ogre::Vector3::UNIT_Y);
						return(fric(r_ij, dir, dir.normalisedCopy(), t_ij));
					}
				}
			}
		}
	}
	return(Ogre::Vector3::ZERO);
}

void AgentController::getAgentForce(Actor *other, Ogre::Vector3 *result)
{
	if(other->myNum != myAgent->myNum)
	{
		int r_ij = myAgent->getRadius() + other->getRadius();
		Ogre::Vector3 d_ij = myAgent->getPosition() - other->getPosition();
		Ogre::Vector3 n_ij = d_ij.normalisedCopy();
		Ogre::Vector3 social = soc(other, r_ij, d_ij, n_ij);
		Ogre::Vector3 attr = att(other, r_ij, d_ij, n_ij);
		(*result) += social + attr;
	}
}

void AgentController::getObstacleForce(Actor *other, Ogre::Vector3 *result)
{
	if(other->myNum != myAgent->myNum)
	{
		int r_i = myAgent->getRadius();

		Ogre::Vector3 dir = other->getPosition() - myAgent->getPosition();
		dir.normalise();

		Ogre::Ray movement(myAgent->getPosition(), dir);
		// from http://www.ogre3d.org/tikiwiki/Intermediate+Tutorial+2&structure=Tutorials
		mRayQuery->setRay(movement);
		Ogre::RaySceneQueryResult &res = mRayQuery->execute();
		Ogre::RaySceneQueryResult::iterator itr = res.begin();
		Ogre::Vector3 d_ik;
		bool foundNearestObstacle = false;
		for(; (foundNearestObstacle == false) && (itr != res.end()); itr++)
		{
			Actor * hit = ActorRegister::getActor(itr->movable);
			if(hit && (hit->myType != this->myAgent->myType))
			{
				foundNearestObstacle = true;
				Ogre::Real amt = itr->distance;
				d_ik = (amt * dir);
			}
		}

		//Ogre::Vector3 d_ik = myAgent->getPosition() - intersectPoint;
		Ogre::Vector3 n_ij = d_ik.normalisedCopy();
		//Ogre::Vector3 social = soc(other, r_ij, d_ij, n_ij);
		//Ogre::Vector3 attr = att(other, r_ij, d_ij, n_ij);
		Ogre::Vector3 obst = obs(other, r_i, d_ik, n_ij);
		Ogre::Vector3 obsattr = obsat(other, r_i, d_ik, n_ij);
		(*result) += obst + obsattr;
	}
}

Ogre::Vector3 AgentController::getForces()
{
	Ogre::Vector3 result = Ogre::Vector3::ZERO;
	result = ((v_d*(e - myAgent->getPosition())) - myAgent->getVelocity())/AgentController::tau;

	list<INode *> *testa = &(((QuadTreeNode *)(myAgent->mNode->parentQuadTreeNode))->sceneChildren);

	list<INode *>::iterator it;
	for(it = testa->begin(); it != testa->end(); it++)
	{
		SceneNode * blah = ((SceneNode *)(*it));
		if(((Actor *)(blah->content))->myType == agent_t)
		{
			getAgentForce((Actor *)blah->content, &result);
		}
		else if(((Actor *)(blah->content))->myType == obstacle_t)
		{
			getObstacleForce((Actor *)blah->content, &result);
		}
	}
	if(result.length() > 50.0f)
	{
		result = result.normalisedCopy() * 50.0f;
	}
	myAgent->setVelocity(result);
	return(result);
}

void AgentController::tick(float deltaTime)
{
	const static float timeBetweenForceUpdates = 0.001f;
	static float timeAgg = timeBetweenForceUpdates;
	static Ogre::Vector3 oldForces;
	Ogre::Vector3 oldpos = myAgent->getPosition();
	mOldpos = oldpos;

	if(!mHasFirstGoal)
	{
		mHasFirstGoal = true;
		list<string *>::iterator first = mGoals.begin();
		if(first != mGoals.end())
		{
			Ogre::Vector2 newGoal = GoalRegister::getSingleton()->getGoal(**first);
			e.x = newGoal.x;
			e.z = newGoal.y;
			delete *first;
			mGoals.pop_front();
		}
		else
		{
			e.x = (rand() % 100) * 1.0f;
			e.z = (rand() % 100) * 1.0f;
		}
	}
	if(!myAgent->getPosition().positionEquals(e, 10.0f))
	{
		float move = v_d * deltaTime;
		if((timeAgg += deltaTime) >= timeBetweenForceUpdates)
		{
			timeAgg = 0.0f;
			oldForces = getForces();
		}
		Ogre::Vector3 newpos = myAgent->getPosition() + ((oldForces + simpleCollide()) * deltaTime);//= myAgent->getPosition() + (myAgent->getVelocity() * deltaTime);
		myAgent->setDirection(newpos - oldpos);
		if(fabs(myAgent->getPosition().squaredDistance(newpos)) > fabs(myAgent->getPosition().squaredDistance(e)))
		{
			myAgent->setVelocity(Ogre::Vector3::ZERO);
			myAgent->setPosition(e);
		}
		else
		{
			myAgent->setPosition(newpos);
		}
		//simpleCollide();
	}
	else
	{
		list<string *>::iterator first = mGoals.begin();
		if(first != mGoals.end())
		{
			Ogre::Vector2 newGoal = GoalRegister::getSingleton()->getGoal(**first);
			e.x = newGoal.x;
			e.z = newGoal.y;
			delete *first;
			mGoals.pop_front();
		}
		else
		{
			e.x = (rand() % 100) * 1.0f;
			e.z = (rand() % 100) * 1.0f;
		}
	}
}
