#pragma once
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#ifdef DEBUG
#define TTY_LOG true
#else
#define TTY_LOG false
#endif

//TODO: make this NightLogger. I keep typing it rong!
enum loglevel_t {log_comment, log_warning, log_error};

//Singleton class
class Nightlogger
{
private:
	Nightlogger(void);
	static ofstream m_logfile;
	static bool logOpen;
	static Nightlogger *m_Singleton;
public:
	~Nightlogger(void);
	static Nightlogger *getSingleton();
	void writeLog(loglevel_t level, string message, ostream &strm);
	void writeLog(loglevel_t level, string message);
	void openLogfile(const char * filename);
	void writeLogfile(loglevel_t level, string message);
};
