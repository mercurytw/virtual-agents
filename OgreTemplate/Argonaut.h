#pragma once
#include <stdlib.h>
#include <iostream>
#include <typeinfo>
#include <string>
#include "rapidjson/document.h"
using namespace std;
using namespace rapidjson;

/**
 * This is a simple wrapper class to validate JSON and return JSON values in a single line.
 *
 **/


class Argonaut
{
private:
	Argonaut(void);
public:
	~Argonaut(void);

	inline static void checkJSONForm(bool expr, string caller)
	{
		if(!expr)
		{
#ifdef DEBUG
			cerr << "Error: " + caller + ": encounered malformed JSON. Failing." << endl;
#endif
			assert(expr);
		}
	}

	template <class T>
	static T getEntry(Value &jsob, string name, string caller)
	{
		void *result;
		checkJSONForm(jsob.HasMember(name.c_str()), caller);
		if(typeid(T) == typeid(double))
		{
			checkJSONForm(jsob[name.c_str()].IsNumber(), caller);
			double r = jsob[name.c_str()].GetDouble(); 
			result = &r;
		}
		else if(typeid(T) == typeid(unsigned int))
		{
			checkJSONForm(jsob[name.c_str()].IsUint(), caller);
			unsigned int r = jsob[name.c_str()].GetUint();
			result = &r;
		}
		else if(typeid(T) == typeid(float))
		{
			checkJSONForm(jsob[name.c_str()].IsNumber(), caller);
			float r = (float)jsob[name.c_str()].GetDouble();
			result = &r;
		}
		else if(typeid(T) == typeid(string *))
		{
			checkJSONForm(jsob[name.c_str()].IsString(), caller);
			string *r = new string(jsob[name.c_str()].GetString());
			result = &r;
		}
		else
		{
#ifdef DEBUG
			cerr << "Failed to find type " + string(typeid(T).name()) + " for " + caller << endl;
#endif
			exit(-1);
		}
		T blah = *static_cast<T *>(result);
		return(blah);
	}

	template <class T>
	static T getEntry(Value &jsob, string caller)
	{
		void *result;
		if(typeid(T) == typeid(double))
		{
			checkJSONForm(jsob.IsNumber(), caller);
			double r = jsob.GetDouble(); 
			result = &r;
		}
		else if(typeid(T) == typeid(unsigned int))
		{
			checkJSONForm(jsob.IsUint(), caller);
			unsigned int r = jsob.GetUint();
			result = &r;
		}
		else if(typeid(T) == typeid(float))
		{
			checkJSONForm(jsob.IsNumber(), caller);
			float r = (float)jsob.GetDouble();
			result = &r;
		}
		else if(typeid(T) == typeid(const char *))
		{
			checkJSONForm(jsob.IsString(), caller);
			string r = string(jsob.GetString());
			result = &r;
		}
		else if(typeid(T) == typeid(string *))
		{
			checkJSONForm(jsob.IsString(), caller);
			string *r = new string(jsob.GetString());
			result = &r;
		}
		else
		{
#ifdef DEBUG
			cerr << "Failed to find type " + string(typeid(T).name()) + " for " + caller << endl;
#endif
			exit(-1);
		}
		T blah = *static_cast<T *>(result);
		return(blah);
	}
	
	
	inline static void checkArray(Value &jsob, string name, unsigned int size, string caller)
	{
		checkJSONForm(jsob.HasMember(name.c_str()), caller);
		checkJSONForm(jsob[name.c_str()].IsArray(), caller);
		checkJSONForm(jsob[name.c_str()].Size() == (SizeType) size, caller);
	}

	inline static void checkArray(Value &jsob, string name, string caller)
	{
		checkJSONForm(jsob.HasMember(name.c_str()), caller);
		checkJSONForm(jsob[name.c_str()].IsArray(), caller);
	}

	inline static void checkArray(Value &jsob, unsigned int size, string caller)
	{
		checkJSONForm(jsob.IsArray(), caller);
		checkJSONForm(jsob.Size() == (SizeType) size, caller);
	}

	inline static void checkObject(Value &jsob, string name, string caller)
	{
		checkJSONForm(jsob.HasMember(name.c_str()), caller);
		checkJSONForm(jsob[name.c_str()].IsObject(), caller);
	}
};
