#pragma once
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

template<class T>
string arbToStr(T arb)
{
	string ostr = "";
	stringstream ostream;
	ostream << arb;
	return(ostream.str());
}