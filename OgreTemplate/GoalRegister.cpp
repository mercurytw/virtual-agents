#include "GoalRegister.h"

GoalRegister * GoalRegister::mSingleton = NULL;
GoalRegister::GoalRegister(void)
{
}

GoalRegister::~GoalRegister(void)
{
	map<string *, Ogre::Vector2>::iterator itr;
	for(itr = mRegister.begin(); itr != mRegister.end(); itr++)
	{
		delete itr->first;
	}
	mRegister.clear();
}

GoalRegister *GoalRegister::getSingleton(void)
{
	if(!mSingleton)
	{
		mSingleton = new GoalRegister();
	}
	return(mSingleton);
}

void GoalRegister::buildRegisterFromJSON(Value &json)
{
	Ogre::Real x,z;
	string caller = "GoalRegister::buildRegisterFromJSON";
	unsigned int arrSize;
	arrSize = (unsigned int) json.Size();
	for(unsigned int i = 0u; i < arrSize; i++)
	{
		Value &goal = json[i];
		Argonaut::checkArray(goal, 2, caller);
		x = (Ogre::Real) Argonaut::getEntry<float>(goal[0u], caller);
		z = (Ogre::Real) Argonaut::getEntry<float>(goal[1u], caller);
		mRegister.insert(pair<string *, Ogre::Vector2>(new string("g" +arbToStr<unsigned int>(i)), Ogre::Vector2(x, z)));
	}
}

void GoalRegister::addGoal(string name, Ogre::Vector2 pos)
{
	map<string *, Ogre::Vector2>::iterator itr;
	for(itr = mRegister.begin(); itr != mRegister.end(); itr++)
	{
		if((itr->first)->compare(name) == 0)
		{
			return;
		}
	}
	mRegister.insert(pair<string *, Ogre::Vector2>(new string(name), pos));
}

Ogre::Vector2 GoalRegister::getGoal(string name)
{
	map<string *, Ogre::Vector2>::iterator itr;
	for(itr = mRegister.begin(); itr != mRegister.end(); itr++)
	{
		if((itr->first)->compare(name) == 0)
		{
			return(itr->second);
		}
	}
	return(Ogre::Vector2::ZERO);
}