#pragma once
#include <list>
#include "INode.h"
using namespace std;


class QuadTreeNode : public INode
{
public:
	QuadTreeNode(void);
	~QuadTreeNode(void);

	list<QuadTreeNode *> quadChildren;
	list<INode *> sceneChildren;
};
