#pragma once

class IController
{
public:
	virtual void tick(float deltaTime) = 0;
	virtual void deinitialize() = 0;
};