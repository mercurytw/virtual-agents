#include "Nightlogger.h"

Nightlogger *Nightlogger::m_Singleton = NULL;
bool Nightlogger::logOpen = false;
ofstream Nightlogger::m_logfile;

Nightlogger::Nightlogger(void)
{
}

Nightlogger::~Nightlogger(void)
{
	if(logOpen)
	{
		m_logfile.close();
	}
	delete m_logfile;
}

Nightlogger *Nightlogger::getSingleton()
{
	if(m_Singleton == NULL)
	{
		m_Singleton = new Nightlogger();
	}

	return(m_Singleton);

}


void Nightlogger::writeLogfile(loglevel_t level, string message)
{
	if(!logOpen)
	{
		cerr << "Warning: Nightlogger::writelog: Cannot write to closed filebuffer" << endl;
		return;
	}

	switch (level)
	{
	case log_comment:
		m_logfile << "Comment: " << message << endl;
		break;
	case log_warning:
		m_logfile << "Warning: " << message << endl;
		break;
	case log_error:
		m_logfile << "Error: " << message << endl;
		break;
	}
}

void Nightlogger::writeLog(loglevel_t level, string message, ostream &strm)
{
	#pragma warning( suppress: 4127 )
	if(((strm == cout) || (strm == cerr)) && !(TTY_LOG))
	{
		if(logOpen)
		{
			writeLogfile(level, message);
		}
		return;
	}

	switch (level)
	{
	case log_comment:
		if(logOpen)
		{
			m_logfile << "Comment: " << message << endl;
		}
		strm << "Comment: " << message << endl;
		break;
	case log_warning:
		if(logOpen)
		{
			m_logfile << "Warning: " << message << endl;
		}
		strm << "Warning: " << message << endl;
		break;
	case log_error:
		if(logOpen)
		{
			m_logfile << "Error: " << message << endl;
		}
		strm << "Error: " << message << endl;
		break;
	}
}

void Nightlogger::writeLog(loglevel_t level, string message)
{
#pragma warning( suppress : 4127 )
	if(!(TTY_LOG))
	{
		if(logOpen)
		{
			writeLogfile(level, message);
		}
		return;
	}

	switch (level)
	{
	case log_comment:
		if(logOpen)
		{
			m_logfile << "Comment: " << message << endl;
		}
		cout << "Comment: " << message << endl;
		break;
	case log_warning:
		if(logOpen)
		{
			m_logfile << "Warning: " << message << endl;
		}
		cout << "Warning: " << message << endl;
		break;
	case log_error:
		if(logOpen)
		{
			m_logfile << "Error: " << message << endl;
		}
		cout << "Error: " << message << endl;
		break;
	}
}

void Nightlogger::openLogfile(const char * filename)
{
	m_logfile.open(filename);
	if(!m_logfile.is_open())
	{
		writeLog(log_error, "Nightlogger::openLogfile: Failed to open file for logging: " + string(filename), cerr);
		return;
	}
	logOpen = true;
}

